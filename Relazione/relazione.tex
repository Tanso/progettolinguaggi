\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{listings}
\author{Luca Tansini\\mat: 920252\\aa 2017/18}
\title{Relazione del Progetto di Linguaggi e Traduttori}
\begin{document}
\maketitle
\tableofcontents
\pagebreak

\section{Analisi Lessicale}
L'analizzatore lessicale è stato sviluppato in JFlex e si trova nel file \texttt{scanner.lex}; la classe generata è \texttt{Scanner.java}.
A livello di analisi lessicale sono stati eliminati i commenti, le righe vuote e le interruzioni di riga specificate con '\&'.
\subsection{Classe ScannerTest}
La classe di test \texttt{ScannerTest.java} elenca i token riconosciuti man mano dallo standard input; è stato aggiunto un minimo di pretty printing per andare a capo sul token CR, che risulta ordinato nel caso di redirezione dell'input da file di codice sorgente.

\section{Analisi Sintattica} \label{analisiSintattica}
Per l'analisi sintattica si è scelto di utilizzare Cup per generare un Abstract Syntax Tree (AST) e il parser si trova nel file \texttt{parser.cup}. Dal file Cup vengono poi generate le classi \texttt{Parser.java} e \texttt{ParserSym.java} che contiene le costanti enumerative corrispondenti ai simboli terminali della grammatica.
Rispetto alla grammatica suggerita nel documento di specifica del progetto è stata effettuata un'unica modifica degna di nota: la produzione "seqIstruzioni $\,\to\,$ seqIstruzioni istruzione cr" è stata trasformata in "seqIstruzioni $\,\to\,$ istruzione cr seqIstruzioni". La modifica è resa possibile dal fatto che le operazioni semantiche, nello specifico il riempimento della symbol table, sono state spostate in una successiva fase di analisi semantica così da poter fare un parsing in profondità del codice. Il vantaggio ottenuto è che il riferimento restituito alla fine del parsing è a tutti gli effetti la radice dell'AST associato al codice sorgente. Di conseguenza in questa fase i nodi dell'albero conterranno i nomi degli identificatori e non dei riferimenti alla symbol table.\\
Nel file di specifica Cup è stato aggiunto il metodo \texttt{line} nella sezione \texttt{parser code} che restituisce il numero di linea dell'ultimo token letto dallo scanner. Viene utilizzato per inserire nell'AST il numero di linea di alcune istruzioni ed espressioni al fine di rendere più chiara la comunicazione di alcuni errori.
\\\\
Vediamo ora in dettaglio le classi utilizzate per la modellazione e costruzione dell'AST.
\subsection{Classe Program}
La classe \texttt{Program} viene restituita dal simbolo iniziale della grammatica e contiene la radice dell'AST, ovvero un riferimento al blocco principale di istruzioni del programma, che può essere ottenuto con il metodo \texttt{getCode}.

\subsection{Classe InstructionSeq}
Questa classe modella un blocco di istruzioni sotto forma di linked list. Grazie alla modifica della grammatica spiegata nella sezione \ref{analisiSintattica} tutte le operazioni sulla lista vengono effettuate in pre-ordine. Le operazioni disponibili in \texttt{InstructionSeq} sono:\\
\begin{itemize}
\item \texttt{fillSymbolTable}: operazione utilizzata in fase di analisi semantica per riempire la symbol table.
\item \texttt{generateCode}: metodo per la generazione del codice.
\item \texttt{codeToString}: metodo di debug per fare pretty printing del blocco di istruzioni. L'indentation level viene utilizzato per stampare i cicli in maniera ordinata, adeguatamente indentati. 
\end{itemize}

\subsection{Classe Instruction}
Il file \texttt{Instruction.java} modella tutte le istruzioni disponibili nel linguaggio. Contiene la classe astratta \texttt{Instruction} che definisce i metodi \texttt{fillSymbolTable}, \texttt{generateCode}, \texttt{codeToString} (con le stesse funzioni già descritte in \texttt{InstructionSeq}) e il campo intero \texttt{line} con il relativo metodo getter (dal significato autoesplicativo) che vengono utilizzati per la gestione degli errori. Il file contiene poi le tre classi concrete \texttt{AssignmentInstruction}, \texttt{PrintInstruction} e  \texttt{LoopInstruction} che implementano le istruzioni del linguaggio.

\subsection{Classe Expr}
La classe astratta \texttt{Expr} modella le espressioni utilizzabili e definisce gli stessi metodi di \texttt{Instruction}. Le classi concrete che la estendono implementano tutte le possibili espressioni del linguaggio.

\section{Analisi Semantica}
L'analisi semantica viene effettuata sull'AST ottenuto dall'analizzatore sintattico. I nodi dell'AST sono costituiti dalle classi descritte nella sezione \ref{analisiSintattica}. L'operazione più importante legata a questa fase è la creazione e il riempimento della symbol table.

\subsection{Symbol Table}
L'implementazione della symbol table si trova nel file \texttt{SymbolTable.java}. La tabella è un vettore di oggetti di tipo \texttt{Descriptor} i quali rappresentano ognuno una variabile. I metodi messi a disposizione sono:
\begin{itemize}
\item \texttt{find}: prende in input il nome di un identificatore e ritorna, se esiste, il descrittore associato a quel nome.
\item \texttt{add}: metodo privato che inserisce il descrittore passato come argomento nella symbol table. Non vengono effettuati controlli su possibili inserimenti duplicati in quanto il metodo è privato e viene chiamato solamente da \texttt{findCreate} che garantisce l'unicità dei nomi degli identificatori.
\item \texttt{findCreate}: cerca nella tabella il descrittore associato al nome preso come argomento, se non c'e' lo aggiunge. Lo scope viene sempre sovrascritto tranne nel caso in cui lo scope presente nella symbol table sia un antenato dello scope corrente.
\item \texttt{printTable}: metodo di debug utilizzato alla fine della compilazione che fa pretty printing della symbol table.
\end{itemize}
La symbol table viene riempita dai metodi \texttt{fillSymbolTable} delle classi \texttt{Instruction} e \texttt{Expr}.

\subsubsection{Classe Descriptor}
La classe \texttt{Descriptor} rappresenta le entry della symbol table. Ad ogni variabile è associato un unico descrittore che contiene i campi:
\begin{itemize}
\item \texttt{ident}: nome dell'identificatore
\item \texttt{address}: indirizzo di memoria assegnato alla variabile
\item \texttt{scope}: il contesto più ampio in cui la variabile è stata assegnata (quindi dichiarata) l'ultima volta. Si è scelto di inserire lo scope delle variabili per sollevare degli errori in compilazione in caso di utilizzo di variabili che potrebbero non essere state inizializzate (poichè dichiarate in cicli o operatori ternari).
\end{itemize}
I metodi definiti in questa classe sono principalmente getter e setter dei campi e il metodo \texttt{equals} che per confrontare due descrittori controlla sia il nome dell'identificatore che lo scope.

\subsubsection{Classe Scope}
Nel file \texttt{Scope.java} è implementata la classe che modella gli scope delle variabili. Si è scelto di implementare gli scope con una struttura gerarchica ad albero in cui ogni nodo conosce solamente il suo parent; lo scope radice, chiamato "global" ha come genitore \texttt{null}. L'unico metodo interessante messo a disposizione da questa classe è \texttt{equalsOrHasBetweenAncestors} che confronta lo scope S preso come argomento con l'oggetto stesso T. Se S è uguale a T stesso restituisce \texttt{true}, altrimenti se T ha uno scope parent chiama ricorsivamente la procedura sul suo parent, passandogli S come argomento, se invece T non ha un parent (perchè è lo scope "global") restituisce \texttt{false}.

\subsubsection{Classe UninitializedVariableUseException}
La classe \texttt{UninitializedVariableUseException} estende \texttt{RuntimeException} e viene utilizzata in fase di analisi semantica per segnalare errori in caso di utilizzo di variabili non inizializzate o potenzialmente non inizializzate. I campi della classe sono:
\begin{itemize}
\item \texttt{varId}: il nome dell'identificatore che ha causato l'errore
\item \texttt{line}: la linea a cui si è varificato l'errore
\item \texttt{context}: di tipo \texttt{Scope}, indica il contesto in cui era stata definita l'ultima volta. Il contesto può essere \texttt{null} e ciò significa che la variabile non è mai stata dichiarata prima dell'uso. 
\end{itemize}
Il metodo \texttt{toString} segnala che la variabile potrebbe non essere stata inizializzata quando il \texttt{context} è diverso da \texttt{null} oppure che la variabile non è definita quando il \texttt{context} è nullo.

\section{Generazione di Codice}
Il codice generato dal compilatore è quello del pacchetto \texttt{ltMacchina} fornito. Come detto nella sezione \ref{analisiSintattica} la generazione del codice viene fatta visitando in profondità l'AST tramite il metodo \texttt{generateCode}.
Chi si occupa del lancio della generazione di codice è la classe eseguibile \texttt{Compiler.}
\subsection{Classe Compiler}
Il file \texttt{Compiler.java} contiene il metodo \texttt{main} che prende in input un file sorgente e svolge tutti i passi della compilazione fino ad ottenere il file eseguibile. In particolare è qui che viene creata la symbol table che viene passata all'AST tramite il metodo \texttt{filSymbolTable} e che viene poi completata assegnando ad ogni variabile indirizzi di memomria incrementali (che partono da 2, poichè le prime due celle sono riservate per i valori tmeporanei usati nel calcolo dell'espressione MOD).
\section{Esempi}
Alcuni esempi di codice sorgente e compilato si trovano rispettivamente nelle cartelle \texttt{SorgentiTest} e \texttt{TestCompilati}. Alcuni di questi sorgenti non compilano in quanto contengono degli errori di utilizzo di variabili non dichiarate o potenzialmente non inizializzate. Ad esempio il file \texttt{ternInitial.src} contiene un errore alla riga 4 poichè si tenta di utilizzare una variabile (\texttt{pari}) che potrebbe non essere stata inizializzata in quanto clausola di un operatore ternario.
\lstset{numbers=left, numberstyle=\small, numbersep=8pt, frame = single, framexleftmargin=15pt}
\begin{lstlisting} 
x = input "x: "
output "x % 2 =  " x % 2 ? dispari = 1 : pari = 0
newLine
output pari
newLine
output dispari
newLine
\end{lstlisting}
Se si tenta di compilarlo si ottiene il seguente errore: \texttt{"Exception in thread "main" Variable pari used at line 4 may be uninitialized (previously defined in scope ternary operator at line 2 branch false)..."}.
\end{document}