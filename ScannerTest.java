//Semplice classe di test che elenca i token riconosciuti dall'analizzatore lessicale (Scanner)

import java.io.*;
import java_cup.runtime.*;

public class ScannerTest{

    public static void main(String[] args) throws IOException {

        ComplexSymbolFactory sf = new ComplexSymbolFactory();
        Scanner scanner = new Scanner(new InputStreamReader(System.in),sf);
        java_cup.runtime.Symbol t;
        while((t = scanner.next_token()).sym != ParserSym.EOF){
            System.out.print((ParserSym.terminalNames[t.sym]) + " ");
            if(t.sym == ParserSym.CR)
                System.out.println();
        }
    }
}
