//Classe che modella gli scope delle variabili con una struttura gerarchica ad albero

public class Scope{

    private Scope parent;
    private String name;

    public Scope(Scope parent, String name){
        this.parent = parent;
        this.name = name;
    }

    //Il metodo equalsOrHasBetweenAncestors ritorna true se il parametro s è uno scope uguale o un antenato
    public boolean equalsOrHasBetweenAncestors(Scope s){
        if(this.equals(s))
            return true;
        if(parent == null)
            return false;
        return parent.equalsOrHasBetweenAncestors(s);
    }

    public String toString(){
        return this.name;
    }

}
