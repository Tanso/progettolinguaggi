//Classe che modella un blocco di istruzioni

import lt.macchina.Codice;
import static lt.macchina.Macchina.*;

class InstructionSeq{

    private Instruction instr;
    private InstructionSeq next;

    public InstructionSeq(Instruction instr, InstructionSeq next){
        this.instr = instr;
        this.next = next;
    }

    public void fillSymbolTable(SymbolTable st, Scope s){
        instr.fillSymbolTable(st,s);
        if(next != null)
            next.fillSymbolTable(st,s);
    }

    public void generateCode(Codice c, SymbolTable st){
        instr.generateCode(c,st);
        if(next != null)
            next.generateCode(c,st);
    }

    public String codeToString(int indentationLevel){
        if(next != null)
            return instr.codeToString(indentationLevel) + next.codeToString(indentationLevel);
        return instr.codeToString(indentationLevel);
    }
}
