//Classe istanziata dal simbolo iniziale della grammatica che funge da radice dell'AST del programma

class Program{

    private InstructionSeq sourceCode;

    public Program(InstructionSeq code){
        this.sourceCode = code;
    }

    public InstructionSeq getCode() {
      return sourceCode;
    }
}
