//Classe che modella la symbol table

import java.util.Vector;
import java.util.Iterator;

public class SymbolTable implements Iterable<Descriptor>{

    private Vector<Descriptor> table; //implementazione basata su Vector

    public SymbolTable(){
        table = new Vector<Descriptor>();
    }

    //Cerca il descrittore di una stringa nella tabella, se non c'e' restituisce null
    public Descriptor find(String id){
        for(Descriptor d: table)
            if(d.getIdent().equals(id))
                return d;
        return null;
    }

    //Aggiunge un descrittore alla tabella
    private void add(Descriptor d){
        table.add(d);
    }

    //Cerca il descrittore di una stringa nella tabella, se non c'e' lo aggiunge. Lo scope viene sempre sovrascritto tranne nel caso in cui lo scope presente nella symbol table sia un antenato dello scope corrente
    public Descriptor findCreate(String id, Scope s){
        Descriptor d = this.find(id);
        if (d == null){
            d = new Descriptor(id,s);
            this.add(d);
        }
        else if(s.equalsOrHasBetweenAncestors(d.getScope())){
            return d;
        }
        else{
            d.setScope(s);
        }
        return d;
    }

    public Iterator<Descriptor> iterator(){
        return table.iterator();
    }

    public void printTable(){
        System.out.println("\n*********SymbolTable*********");
        for(Descriptor d: table)
            System.out.println(d.getIdent() + " \"" + d.getScope() + "\" " + d.getAddress());
        System.out.println("*****************************\n");
    }

}
