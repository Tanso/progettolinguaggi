//Classe che modella le espressioni

import lt.macchina.Codice;
import static lt.macchina.Macchina.*;

//Classe astratta che modella le espressioni
abstract class Expr{
    public abstract void fillSymbolTable(SymbolTable st, Scope s);
    public abstract void generateCode(Codice c, SymbolTable st);
    public abstract String codeToString();
}

//Classi concrete che modellano le espressioni
class NumExpr extends Expr{
    private Integer num;

    public NumExpr(Integer num){
        this.num = num;
    }

    public void fillSymbolTable(SymbolTable st, Scope s){
        return;
    }

    public void generateCode(Codice c, SymbolTable st){
        c.genera(PUSHIMM, num.intValue());
    }

    public String codeToString(){
        return num.toString();
    }
}

class IdExpr extends Expr{
    private String id;
    private int line;

    public IdExpr(String id, int line){
        this.id = id;
        this.line = line;
    }

    //se lo scope in cui è stata definita la variabile non è lo stesso o un antenato dello scope attuale solleva un'eccezione
    public void fillSymbolTable(SymbolTable st, Scope s) throws UninitializedVariableUseException{
        Descriptor d = st.find(id);
        if(d == null)
            throw new UninitializedVariableUseException(id,line,null);
        if(!s.equalsOrHasBetweenAncestors(d.getScope()))
            throw new UninitializedVariableUseException(id,line,d.getScope());
    }

    public void generateCode(Codice c, SymbolTable st){
        c.genera(PUSH, st.find(id).getAddress());
    }

    public String codeToString(){
        return id;
    }
}

class AddExpr extends Expr{
    private Expr left, right;

    public AddExpr(Expr left, Expr right){
        this.left = left;
        this.right = right;
    }

    public void fillSymbolTable(SymbolTable st, Scope s){
        left.fillSymbolTable(st,s);
        right.fillSymbolTable(st,s);
    }

    public void generateCode(Codice c, SymbolTable st){
        left.generateCode(c,st);
        right.generateCode(c,st);
        c.genera(ADD);
    }

    public String codeToString(){
        return left.codeToString() + " + " + right.codeToString();
    }
}

class SubExpr extends Expr{
    private Expr left, right;

    public SubExpr(Expr left, Expr right){
        this.left = left;
        this.right = right;
    }

    public void fillSymbolTable(SymbolTable st, Scope s){
        left.fillSymbolTable(st,s);
        right.fillSymbolTable(st,s);
    }

    public void generateCode(Codice c, SymbolTable st){
        left.generateCode(c,st);
        right.generateCode(c,st);
        c.genera(SUB);
    }

    public String codeToString(){
        return left.codeToString() + " - " + right.codeToString();
    }
}

class MulExpr extends Expr{
    private Expr left, right;

    public MulExpr(Expr left, Expr right){
        this.left = left;
        this.right = right;
    }

    public void fillSymbolTable(SymbolTable st, Scope s){
        left.fillSymbolTable(st,s);
        right.fillSymbolTable(st,s);
    }

    public void generateCode(Codice c, SymbolTable st){
        left.generateCode(c,st);
        right.generateCode(c,st);
        c.genera(MUL);
    }

    public String codeToString(){
        return left.codeToString() + " * " + right.codeToString();
    }
}

class DivExpr extends Expr{
    private Expr left, right;

    public DivExpr(Expr left, Expr right){
        this.left = left;
        this.right = right;
    }

    public void fillSymbolTable(SymbolTable st, Scope s){
        left.fillSymbolTable(st,s);
        right.fillSymbolTable(st,s);
    }

    public void generateCode(Codice c, SymbolTable st){
        left.generateCode(c,st);
        right.generateCode(c,st);
        c.genera(DIV);
    }

    public String codeToString(){
        return left.codeToString() + " / " + right.codeToString();
    }
}

class ModExpr extends Expr{
    private Expr a, b;

    public ModExpr(Expr a, Expr b){
        this.a = a;
        this.b = b;
    }

    public void fillSymbolTable(SymbolTable st, Scope s){
        a.fillSymbolTable(st,s);
        b.fillSymbolTable(st,s);
    }

    public void generateCode(Codice c, SymbolTable st){
        a.generateCode(c,st);
        b.generateCode(c,st);
        c.genera(POP, 1); //mette il risultato di b in R_b
        c.genera(POP, 0); //mette il risultato di a in R_a
        //l'operazione da eseguire è a - b * (a / b)
        c.genera(PUSH, 0);
        c.genera(PUSH, 1);
        c.genera(PUSH, 0);
        c.genera(PUSH, 1);
        c.genera(DIV);
        c.genera(MUL);
        c.genera(SUB);
    }

    public String codeToString(){
        return a.codeToString() + " % " + b.codeToString();
    }
}

class UnPlusExpr extends Expr{
    private Expr e;

    public UnPlusExpr(Expr e){
        this.e = e;
    }

    public void fillSymbolTable(SymbolTable st, Scope s){
        e.fillSymbolTable(st,s);
    }

    public void generateCode(Codice c, SymbolTable st){
        e.generateCode(c,st);
    }

    public String codeToString(){
        return "+" + e.codeToString();
    }
}

class UnMinusExpr extends Expr{
    private Expr e;

    public UnMinusExpr(Expr e){
        this.e = e;
    }

    public void fillSymbolTable(SymbolTable st, Scope s){
        e.fillSymbolTable(st,s);
    }

    public void generateCode(Codice c, SymbolTable st){
        c.genera(PUSHIMM, 0);
        e.generateCode(c,st);
        c.genera(SUB);
    }

    public String codeToString(){
        return "-" + e.codeToString();
    }
}

class AssignExpr extends Expr{
    private String id;
    private Expr e;

    public AssignExpr(String id, Expr e){
        this.id = id;
        this.e = e;
    }

    public void fillSymbolTable(SymbolTable st, Scope s){
        e.fillSymbolTable(st,s);
        st.findCreate(id,s);
    }

    public void generateCode(Codice c, SymbolTable st){
        e.generateCode(c,st);
        c.genera(POP, st.find(id).getAddress());
        c.genera(PUSH, st.find(id).getAddress());
    }

    public String codeToString(){
        return id + " = " + e.codeToString();
    }
}

class InputExpr extends Expr{
    private String prompt;

    public InputExpr(String prompt){
        this.prompt = prompt;
    }

    public void fillSymbolTable(SymbolTable st, Scope s){
        return;
    }

    public void generateCode(Codice c, SymbolTable st){
        if(prompt != null)
            for(int i=0; i<prompt.length(); i++){
                c.genera(PUSHIMM, prompt.charAt(i));
                c.genera(OUTPUTCH);
            }
        c.genera(INPUT);
    }

    public String codeToString(){
        return "input" + (prompt != null ? " \"" + prompt + "\" " : "");
    }
}

class TernaryExpr extends Expr{
    private Expr cond,a,b;
    private int line;

    public TernaryExpr(Expr cond, Expr a, Expr b, int line){
        this.cond = cond;
        this.a = a;
        this.b = b;
        this.line = line;
    }

    public void fillSymbolTable(SymbolTable st, Scope s){
        cond.fillSymbolTable(st,s);
        //vengono creati due diversi scope perchè potrebbero essere presenti delle espressioni di assegnamento nei due rami del'operatore ternario
        a.fillSymbolTable(st,new Scope(s,"ternary operator at line " + line + " branch true"));
        b.fillSymbolTable(st,new Scope(s,"ternary operator at line " + line + " branch false"));
    }

    public void generateCode(Codice c, SymbolTable st){
        cond.generateCode(c,st);
        int jaddr = c.generaParziale(JZERO);
        a.generateCode(c,st);
        int jendaddr = c.generaParziale(JUMP);
        c.completaIstruzione(jaddr, c.indirizzoProssimaIstruzione());
        b.generateCode(c,st);
        c.completaIstruzione(jendaddr, c.indirizzoProssimaIstruzione());
    }

    public String codeToString(){
        return cond.codeToString() + " ? " + a.codeToString() + " : " + b.codeToString();
    }
}
