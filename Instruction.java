//Classe che modella le istruzioni del linguaggio

import lt.macchina.Codice;
import static lt.macchina.Macchina.*;

//Classe astratta che modella le istruzioni
abstract class Instruction{
    public abstract void fillSymbolTable(SymbolTable st, Scope s);
    public abstract void generateCode(Codice c, SymbolTable st);
    public abstract String codeToString(int indentationLevel);
    public int line;

    public int getLine(){
        return line;
    }
}

//Classi concrete che modellano le istruzioni
class AssignmentInstruction extends Instruction{

    private String id;
    private Expr expr;

    public AssignmentInstruction(String id, Expr expr, int line){
        this.id = id;
        this.expr = expr;
        this.line = line;
    }

    public void fillSymbolTable(SymbolTable st, Scope s){
        expr.fillSymbolTable(st, s);
        st.findCreate(id, s);
    }

    public void generateCode(Codice c, SymbolTable st){
        expr.generateCode(c,st);
        c.genera(POP, st.find(id).getAddress());
    }

    public String codeToString(int indentationLevel){
        String out = "";
        for(int i=0;i<indentationLevel;i++)
            out += "\t";
        return out + id + " = " + expr.codeToString() + "\n";
    }
}

class PrintInstruction extends Instruction{

    private String str;
    private Expr expr;

    public PrintInstruction(String str, Expr expr, int line){
        this.str = str;
        this.expr = expr;
        this.line = line;
    }

    public void fillSymbolTable(SymbolTable st, Scope s){
        if(expr != null)
            expr.fillSymbolTable(st,s);
    }

    public void generateCode(Codice c, SymbolTable st){
        if(str != null)
            for(int i=0; i<str.length(); i++){
                c.genera(PUSHIMM, str.charAt(i));
                c.genera(OUTPUTCH);
            }
        if(expr != null){
            expr.generateCode(c,st);
            c.genera(OUTPUT);
        }
        if(str == null && expr == null){
            c.genera(PUSHIMM, '\n');
            c.genera(OUTPUTCH);
        }
    }

    public String codeToString(int indentationLevel){
        String out = "";
        for(int i=0;i<indentationLevel;i++)
            out += "\t";
        if(str != null || expr != null){
            out = "output ";
            out += str != null ? "\"" + str +"\" " : "";
            out += expr != null ? expr.codeToString() + "\n" : "\n";
        }
        else
            out += "newLine\n";
        return out;
    }
}

class LoopInstruction extends Instruction{

    private Expr cond;
    private InstructionSeq body;

    public LoopInstruction(Expr cond, InstructionSeq body, int line){
        this.cond = cond;
        this.body = body;
        this.line = line;
    }

    public void fillSymbolTable(SymbolTable st, Scope s){
        cond.fillSymbolTable(st, s);
        Scope nested = new Scope(s, "loop at line " + line);
        body.fillSymbolTable(st,nested);
    }

    public void generateCode(Codice c, SymbolTable st){
        int start = c.indirizzoProssimaIstruzione();
        cond.generateCode(c,st);
        int jaddr = c.generaParziale(JZERO);
        body.generateCode(c,st);
        c.genera(JUMP, start);
        int end = c.indirizzoProssimaIstruzione();
        c.completaIstruzione(jaddr, end);
    }

    public String codeToString(int indentationLevel){
        String out = "";
        for(int i=0;i<indentationLevel;i++)
            out += "\t";
        out += "loop " + cond.codeToString() + "\n";
        out += body.codeToString(indentationLevel+1);
        out += "endLoop\n";
        return out;
    }
}
