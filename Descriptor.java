//Classe che modella le entry della symbolTable, ogni entry si riferisce ad una variabile

public class Descriptor{

    private String ident;
    private int address;
    private Scope scope;

    public Descriptor(String id, int addr, Scope scope){
        ident = id;
        address = addr;
        this.scope = scope;
    }

    public Descriptor(String id, Scope scope){
        this(id, 0, scope);
    }

    public Scope getScope(){
        return scope;
    }

    public void setScope(Scope s){
        this.scope = s;
    }

    public String getIdent(){
        return ident;
    }

    public int getAddress(){
        return address;
    }

    //Il metodo setAddress restituisce l'indirizzo successivo
    public int setAddress(int addr){
        address = addr;
        return address + 1;
    }

    public boolean equals(Descriptor d){
        return this.ident.equals(d.ident) && this.scope.equals(d.scope);
    }

    public boolean equals(Object o){
        if (o instanceof Descriptor)
            return equals((Descriptor) o);
        else
            return false;
    }

    public String toString(){
        return ident + " " + address + " " + 1;
    }
}
