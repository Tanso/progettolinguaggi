//Classe che modella le eccezioni sollevate durante la compilazione in caso di utilizzo di variabili [potenzialmente] non inizializzate

class UninitializedVariableUseException extends RuntimeException{

    private String varId;
    private Scope context;
    private int line;

    public UninitializedVariableUseException(String id, int line, Scope context){
        super();
        varId = id;
        this.line = line;
        this.context = context;
    }

    //Se il contesto è nullo significa che la variabile viene utilizzata senza mai essere stata inizializzata prima, altrimenti lo scope in cui è stata inizializzata non è lo stesso o un antenato del contesto in cui si tenta di utilizzarla
    public String toString(){
        if(context == null)
            return "Variable " + varId + " used at line " + line + " is undefined";
        else
            return "Variable " + varId + " used at line " + line + " may be uninitialized (previously defined in scope " + context + ")";
    }
}
