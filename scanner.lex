//Analizzatore lessicale per il linguaggio del progetto di Linguaggi e Traduttori AA 2017/18

import java_cup.runtime.*;

%%

%unicode

LETTER = [:letter:]
DIGIT = [:digit:]
HEX_DIGIT = [ABCDEFabcdef] | {DIGIT}
ENDLINE = \r | \n
SPACE = [ \t\f]
COMMENT = "//"[^\n\r]*
CONST_STR = "\""("\\\""|[^\n\r\"])+"\""

%cup
%class Scanner

%{
    public int currentLineNumber() {
        return yyline + 1;
    }
%}

%line

%{
    //codice per associare la Symbol Factory
    ComplexSymbolFactory sf;
    public Scanner(java.io.Reader in, ComplexSymbolFactory sf) {
        this(in);
        this.sf = sf;
    }
%}

%%
{DIGIT}+                            {return sf.newSymbol("INT",ParserSym.INT,Integer.parseInt(yytext()));}
"0x"{HEX_DIGIT}+                    {return sf.newSymbol("INT",ParserSym.INT,Integer.parseInt(yytext(),16));}
"("                                 {return sf.newSymbol("LEFT_BRACKET",ParserSym.LEFT_BRACKET);}
")"                                 {return sf.newSymbol("RIGHT_BRACKET",ParserSym.RIGHT_BRACKET);}
"+"                                 {return sf.newSymbol("PLUS",ParserSym.PLUS);}
"-"                                 {return sf.newSymbol("MINUS",ParserSym.MINUS);}
"*"                                 {return sf.newSymbol("MUL",ParserSym.MUL);}
"/"                                 {return sf.newSymbol("DIV",ParserSym.DIV);}
"%"                                 {return sf.newSymbol("MOD",ParserSym.MOD);}
"="                                 {return sf.newSymbol("EQUAL",ParserSym.EQUAL);}
":"                                 {return sf.newSymbol("COLON",ParserSym.COLON);}
"?"                                 {return sf.newSymbol("QUESTION",ParserSym.QUESTION);}
"input"                             {return sf.newSymbol("INPUT",ParserSym.INPUT);}
"output"                            {return sf.newSymbol("OUTPUT",ParserSym.OUTPUT);}
"newLine"                           {return sf.newSymbol("NEW_LINE",ParserSym.NEW_LINE);}
"loop"                              {return sf.newSymbol("LOOP",ParserSym.LOOP);}
"endLoop"                           {return sf.newSymbol("ENDLOOP",ParserSym.ENDLOOP);}
{CONST_STR}                         {return sf.newSymbol("CONST_STR",ParserSym.CONST_STR,yytext().substring(1,yylength()-1));}
^{COMMENT}{ENDLINE}                 {}
^{ENDLINE}                          {}
{COMMENT}                           {}
"&"({SPACE}|{COMMENT})*{ENDLINE}    {}
{LETTER}({DIGIT}|{LETTER})*         {return sf.newSymbol("IDENT",ParserSym.IDENT,yytext());}
{ENDLINE}                           {return sf.newSymbol("CR",ParserSym.CR);}
{SPACE}                             {}
.                                   {return sf.newSymbol("error",ParserSym.error);}
<<EOF>>                             {return sf.newSymbol("EOF", ParserSym.EOF);}
