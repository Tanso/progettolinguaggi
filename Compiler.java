//Compilatore per il linguaggio del progetto del corso di Linguaggi e Traduttori AA 2017/18

import java_cup.runtime.*;
import java.io.*;
import lt.macchina.*;
import static lt.macchina.Macchina.*;

class Compiler {

    public static void main(String[] args) throws Exception {

        if(args.length < 1){
            System.out.println("usage: Compiler <sourceFile> [-o <outfile>]");
            System.exit(-1);
        }

        //gestione dell'opzione per la rinominazione del file compilato in output
        String outfile = "eseguibile";
        if(args.length >= 3)
            if(args[1].equals("-o"))
                outfile = args[2];
            else
                System.out.println("Unknown option " + args[1] + " " + args[2]);

        //creazione del canale di input
        BufferedReader in = new BufferedReader(new FileReader(args[0]));

        //creazione della symbol factory
        ComplexSymbolFactory sf = new ComplexSymbolFactory();

        //creazione dell'analizzatore lessicale
        Scanner scanner = new Scanner(in, sf);

        //creazione del parser
        Parser p = new Parser(scanner, sf);

        //esecuzione del parser che ritorna l'AST
        Symbol res = p.parse();
        Program ast = (Program) res.value;

        //DEBUG stampa del codice parsato
        //System.out.print(ast.getCode().codeToString(0));

        //la symbolTable viene creata e riempita in fase di analisi semantica, così da poter gestire gli scope
        SymbolTable st = new SymbolTable();
        ast.getCode().fillSymbolTable(st, new Scope(null,"global"));

        //assegna gli indirizzi alle variabili
        //riserva spazio per i due registri usati dall'operazione mod
        int nextAddr = 2;
        for(Descriptor d : st)
            nextAddr = d.setAddress(nextAddr);

        //DEBUG stampa della symbol table
        st.printTable();

        //generazione del codice
        Codice c = new Codice(outfile);
        ast.getCode().generateCode(c,st);

        //genera il codice per terminare l'esecuzione
        c.genera(HALT);

        c.fineCodice();
        System.out.println("code written to: " + outfile);
    }
}
